import { Component, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EmailsPopupComponentComponent } from './emails-popup-component/emails-popup-component.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
   encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'Notificatoins';
  minDate: Date;
  maxDate: Date;

   modes: Status[] = [
    {value: true, viewValue: 'Valid'},
    {value: false, viewValue: 'Invalid'},
    
  ];

  constructor(public dialog: MatDialog)
  {
    const currentYear = new Date().getFullYear();
    this.minDate = new Date(currentYear - 20, 0, 1);
    this.maxDate = new Date(currentYear + 1, 11, 31);
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(EmailsPopupComponentComponent, {
      width: '40%',
      height:'100%',
      position:{'top':'0',right:'0'},
      disableClose:true
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }
 
}

interface Status {
  value: boolean;
  viewValue: string;
}
