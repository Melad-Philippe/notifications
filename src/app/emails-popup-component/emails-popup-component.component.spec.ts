import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailsPopupComponentComponent } from './emails-popup-component.component';

describe('EmailsPopupComponentComponent', () => {
  let component: EmailsPopupComponentComponent;
  let fixture: ComponentFixture<EmailsPopupComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailsPopupComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailsPopupComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
