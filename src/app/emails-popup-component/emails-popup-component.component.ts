import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { AppComponent } from '../app.component';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-emails-popup-component',
  templateUrl: './emails-popup-component.component.html',
  styleUrls: ['./emails-popup-component.component.css']
})
export class EmailsPopupComponentComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<AppComponent>) { }
  displayedColumns: string[] = ['email'];
  dsEmails1 = new MatTableDataSource(ELEMENT_DATA);
  dsEmails2 = new MatTableDataSource(ELEMENT_DATA);
  selectedRowIndex1 :number= -1;
  selectedRowIndex2 : number = -1
  ngOnInit(): void {
    
  }

  // highlightEmails1(row) {
  //   debugger
  //   this.selectedRowIndexEmails1 = row.id;
  // }
}

const ELEMENT_DATA: Email[] = [
  { email: 'email1@company.com' ,id:1},
  { email: 'email2@company.com' ,id:2},
  { email: 'email3@company.com' ,id:3},
  { email: 'email4@company.com' ,id:4},
  { email: 'email5@company.com' ,id:5},
  { email: 'email6@company.com' ,id:6},
  { email: 'email7@company.com' ,id:7},
  { email: 'email8@company.com' ,id:8},
  { email: 'email9@company.com' ,id:9}
];
export interface Email {
  email: string;
  id: number;
}